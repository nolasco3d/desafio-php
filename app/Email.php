<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{

    // Sort array of emails
    public function sort($data){
        natsort($data);
        return $data;
    }

    // Filter emails
    public function filter($data){
        $emails = explode(PHP_EOL, $data);
        $valid_emails = [];
        for ($i=0; $i < count($emails)-1; $i++) { 
            if(filter_var($emails[$i], FILTER_VALIDATE_EMAIL)) {
                array_push($valid_emails, $emails[$i]);
            }
        }

        return $valid_emails;
    }
}
