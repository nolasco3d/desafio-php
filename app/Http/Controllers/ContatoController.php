<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Contato;

class ContatoController extends Controller
{
    public function index() {
        $contatos = [
            (object)["nome"=>"Maria", "tel"=>"32153215"],
            (object)["nome"=>"Joao", "tel"=>"98749874"]
        ];
        $contato = new Contato();
        $con = $contato->lista();
        dd($con->nome);
        return view('contato.index', compact('contatos'));
    }

    public function criar(Request $req) {
        dd($req->all());
        return 'Esse é o CRIAR do ContatoController';
    }

    public function editar() {
        return 'Esse é o EDITAR do ContatoController';
    }
}
