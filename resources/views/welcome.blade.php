<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <div>
            @if (count($errors) > 0)
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div>
                    $message
                </div>
            @endif
            <form action="/email/add" method="post">
                {{ csrf_field() }} 
                <label>Message:</label>
                <textarea 
                    type="textarea" 
                    name="message" 
                    placeholder="">
                    </textarea>

                <button>Enviar</button> 
            </form>
                    
        </div>
    </body>
</html>
